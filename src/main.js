import { createApp } from 'vue'
import App from './App.vue'

import 'vuetify/styles'
import {routers} from './routes';
import { createVuetify } from 'vuetify'
import * as components from 'vuetify/components'
import * as directives from 'vuetify/directives'

const vuetify = createVuetify({
    components,
    directives,
})

createApp(App).use(vuetify).use(routers).mount('#app')
// createApp(App).use(vuetify).use(routers).mount('#app')
