import {createRouter, createWebHistory} from 'vue-router';
import MainPage from '@/pages/main.vue'
import UpdatePage from '@/pages/update.vue'

const routerHistory = createWebHistory();

export const routers = createRouter({
    history: routerHistory,
    routes: [
        {
            path: '/',
            name: 'MainPage',
            component: MainPage
        },
        {
            path: '/:id',
            name: 'UpdatePage',
            component: UpdatePage
        },
    ]
})
